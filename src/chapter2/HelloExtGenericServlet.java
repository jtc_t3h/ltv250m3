package chapter2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class HelloExtGenericServlet extends GenericServlet {

	@Override
	public void init() throws ServletException {
		System.out.println("HelloExtGenericServlet: init method.");
	}
	
	 @Override
	public void destroy() {
		System.out.println("HelloExtGenericServlet: destroy method.");
	}
	
	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = res.getWriter();
		out.println("<h1>Hello Servlet!</h1>");
		out.println("<h3>Create servlet by extends GenericServlet class.<h3>");
	}

}
