package chapter2;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/chapter2/login.html")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/pages/chapter2/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Lay du lieu nguoi dung nhap
				String username = request.getParameter("username");
				String password = request.getParameter("password");
				
				// Kiem tra dang nhap thanh cong hay khong?
				if (username != null && password != null && username.equalsIgnoreCase(password)) {
					System.out.println("Login successfull.");
					
					HttpSession session = request.getSession();
					session.setAttribute("username", username);
					
					response.sendRedirect("../pages/chapter2/home.jsp");
				} else {
					System.out.println("Login fail.");
					
					request.setAttribute("message", "Username or Password incorrect.");
					request.getRequestDispatcher("../pages/chapter2/login.jsp").forward(request, response);
				}
	}

}
