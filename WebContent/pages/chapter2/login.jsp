<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
  <% if (request.getAttribute("message") != null){ %>
    <h3 style="color: red;"><%=request.getAttribute("message") %></h3> 
  <%} %>
  ${message != null?message:'' }

  <form method="post"><!-- action="../../chapter2/login.html"  --> 
    <label>UserName:</label><input type="text" name="username"><br>
    <label>Password:</label><input type="password" name="password"><br>
    <button>Login</button>
  </form>
</body>
</html>